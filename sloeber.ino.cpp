#ifdef __IN_ECLIPSE__
//This is a automatic generated file
//Please do not modify this file
//If you touch this file your change will be overwritten during the next build
//This file has been generated on 2018-03-03 16:40:15

#include "Arduino.h"
#include "Arduino.h"
#include "BluefruitConfig.h"
#include <Adafruit_GFX.h>
#include <Adafruit_IS31FL3731.h>
#include "Adafruit_LEDBackpack.h"
#include "Adafruit_BluefruitLE_SPI.h"

void setup() ;
void loop() ;
void log(const __FlashStringHelper *msg) ;
void error(const __FlashStringHelper*err) ;
void initBluefruitLE() ;
void initMatrix8x16() ;
void setMatrix8x16Brightness(uint16_t brightness) ;
void initLEDWing105() ;
void setLEDWing105Brightness(uint16_t brightness) ;
void waveLEDWing105(int index, int brightness) ;

#include "Adafruit_feather_light_up.ino"


#endif
