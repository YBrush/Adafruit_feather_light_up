/*
 * Adafruit_feather_light_up.ino
 *
 * Controls a led-wing which is connected to the feather board by using the
 * "Controller -> Color Picker" dialog of the Adafruit "Bluefruit LE Connect" app.
 * Only brightness is supported.
 *
 * To automatically detect the mounted wing it is necessary to interconnect pin 5 and pin 6
 * on wing LEDWing105 and not to do so on wing Matrix 8x16!
 *
 * If Bluetooth is never connected the Matrix will be switched off after a timeout of 5 minutes.
 *
 *  Created on: 17.02.2018
 *      Author: Mario
 */

/*
 * Includes
 */
#include "Arduino.h"
#include "BluefruitConfig.h"
#include <Adafruit_GFX.h>
#include <Adafruit_IS31FL3731.h>
#include "Adafruit_LEDBackpack.h"
#include "Adafruit_BluefruitLE_SPI.h"

/*
 * Function prototypes over in packetparser.cpp
 */
uint8_t readPacket(Adafruit_BLE *ble, uint16_t timeout);
float parsefloat(uint8_t *buffer);
void printHex(const uint8_t * data, const uint32_t numBytes);
extern uint8_t packetbuffer[];		// the packet buffer

/*
 * Defines
 */
//#define COM_MONITOR_ON
#define BTLE_FACTORYRESET_ENABLE         1
#define BTLE_MINIMUM_FIRMWARE_VERSION    "0.6.6"
#define BTLE_MODE_LED_BEHAVIOUR          "SPI" // default is MODE

static const int PIN_DRIVE_LOW = 5;
static const int PIN_SEL_TARGET = 6;

static const int TARGET_MATRIX8x16 = 0;
static const int TARGET_LEDWING105 = 1;

static const long TIMEOUT_TURNOFF_MATRIX = 300000;		// after this time [ms] the matrix will be turned off

static const int WAVE[] = {4, 8, 12, 16, 24, 32, 40, 60, 80, 120, 160, 240, 240, 160, 120, 80, 60, 40, 32, 24, 16, 12, 8, 4};
//static const int WAVE[] = {1, 2, 3, 4, 6, 8, 10, 15, 20, 30, 40, 60, 60, 40, 30, 20, 15, 10, 8, 6, 4, 3, 2, 1};
//static const int WAVE[] = {1, 4, 17, 37, 64, 95, 128, 161, 192, 219, 239, 251, 255, 251, 239, 219, 192, 161, 128, 95, 64, 38, 17, 4};
static const int WAVE_LENGTH = 24;

/*
 * Variables
 */
Adafruit_BluefruitLE_SPI *pBluefruitLE;
Adafruit_8x16minimatrix *pMatrix8x16;
Adafruit_IS31FL3731_Wing *pLEDWing105;
uint8_t mPacketLen = 0;
int mBrightness = 255;
int mWaveIndex = 0;
int mTarget = TARGET_MATRIX8x16;
bool mIsTimeoutActive = true;
bool mIsDisplayOn = true;


/**
 * The setup function is called once at startup of the sketch
 */
void setup()
{
	// pins
	pinMode(LED_BUILTIN, OUTPUT);
	digitalWrite(LED_BUILTIN, LOW);

	// select the hw/wing
	digitalWrite(PIN_DRIVE_LOW, LOW);
	pinMode(PIN_DRIVE_LOW, OUTPUT);
	pinMode(PIN_SEL_TARGET, INPUT_PULLUP);

	// serial interface
	Serial.begin(115200);
	Serial.println("Feather_light_up.ino v0.1");
	Serial.println("-------------------------");

	// initialize the BluefruitLE
	initBluefruitLE();

	// select the mounted wing
	if (digitalRead(PIN_SEL_TARGET) == LOW)
			mTarget = TARGET_LEDWING105;

	// initialize the 8x16 led matrix
	if (mTarget == TARGET_MATRIX8x16)
		initMatrix8x16();

	// initialize the led wing 105
	if (mTarget == TARGET_LEDWING105)
		initLEDWing105();

}


/**
 * The loop function is called in an endless loop
 */
void loop()
{
	// makes the activity led at the BTLE modul flash
	//delay(100);

	// Wait for new data to arrive from BluefruitLE
	mPacketLen = readPacket(pBluefruitLE, 180);
	if (mPacketLen > 0) {
		// turn off the timeout
		mIsTimeoutActive = false;
		mIsDisplayOn = true;

		// read the Colors and calculate the mean
		if (packetbuffer[1] == 'C') {
			uint8_t red = packetbuffer[2];
			uint8_t green = packetbuffer[3];
			uint8_t blue = packetbuffer[4];
			mBrightness = (red + green + blue) / 3;

			#ifdef COM_MONITOR_ON
			Serial.print("Brightness set to ");
			Serial.println(mBrightness, DEC);
		#endif
		}

		// set new brightness to 16x8 Matrix
		switch (mTarget) {
			case TARGET_MATRIX8x16:
				setMatrix8x16Brightness(mBrightness);
				break;
			case TARGET_LEDWING105:
				// it already shows the wave
				break;
			default:
				error(F("Unsupported target!"));
				break;
		}
	}

	// shows a wave
	if ((mTarget == TARGET_LEDWING105)  && mIsDisplayOn) {
		waveLEDWing105(mWaveIndex++, mBrightness);

		// reset
		if (mWaveIndex >= (WAVE_LENGTH - 1))
			mWaveIndex = 0;

	}


	// turns off the matrix after the timeout [ms]
	if (mIsTimeoutActive && (millis() > TIMEOUT_TURNOFF_MATRIX)) {
		// turns on the whole matrix
		switch (mTarget) {
			case TARGET_MATRIX8x16:
				pMatrix8x16->clear();
				pMatrix8x16->writeDisplay();
				break;
			case TARGET_LEDWING105:
				pLEDWing105->clear();
				break;
			default:
				error(F("Unsupported target!"));
				break;
		}

		mIsTimeoutActive = false;
		mIsDisplayOn = false;
	}

}


/**
 * Prints the given msg to the serial port if the COM_MONITOR_ON is defined.
 */
void log(const __FlashStringHelper *msg) {
#ifdef COM_MONITOR_ON
	Serial.print("Log: ");
	Serial.println(msg);
#endif
}


/**
 * Prints the error message and stops the execution of any further code.
 */
void error(const __FlashStringHelper*err) {
	Serial.print("Error: ");
	Serial.println(err);

	// endless
	while (1);
}


/**
 * Creates the Adafruit_BluefruitLE_SPI object and runs the initialization.
 */
void initBluefruitLE() {

	// new object for interacting with the Bluetooth LE module
	pBluefruitLE = new Adafruit_BluefruitLE_SPI(BLUEFRUIT_SPI_CS, BLUEFRUIT_SPI_IRQ, BLUEFRUIT_SPI_RST);

	// Initialize the BTLE module
	log(F("Initializing the Bluefruit LE module: "));

	if (!pBluefruitLE->begin(VERBOSE_MODE)) {
	    error(F("Couldn't find Bluefruit, make sure it's in CoMmanD mode & check wiring?"));
	}
	log(F("OK!"));

	// Perform a factory reset to make sure everything is in a known state
	if (BTLE_FACTORYRESET_ENABLE) {
		log(F("Performing a factory reset: "));
	    if (!pBluefruitLE->factoryReset()) {
	      error(F("Couldn't factory reset"));
	    }
	}

	// Disable command echo from Bluefruit
	pBluefruitLE->echo(false);

	// Print Bluefruit information
	log(F("Requesting Bluefruit info:"));
	pBluefruitLE->info();

	// Print some information's
	log(F("Please use Adafruit Bluefruit LE app to connect in"));
	log(F("controller -> color picker mode."));
	log(F(""));

	// debug info is a little annoying after this point!
	pBluefruitLE->verbose(false);

	// Set BluefruitLE to DATA mode
	log(F("Switching to DATA mode!"));
	pBluefruitLE->setMode(BLUEFRUIT_MODE_DATA);

	// LED Activity command is only supported from 0.6.6
	if (pBluefruitLE->isVersionAtLeast(BTLE_MINIMUM_FIRMWARE_VERSION)) {
		// Change Mode LED Activity
	    log(F("******************************"));
	    log(F("Change LED activity to " BTLE_MODE_LED_BEHAVIOUR));
	    pBluefruitLE->sendCommandCheckOK("AT+HWModeLED=" BTLE_MODE_LED_BEHAVIOUR);
	    log(F("******************************"));
	}

}


/**
 * Creates the Adafruit_8x16minimatrix object and runs the initialization.
 */
void initMatrix8x16() {
	pMatrix8x16 = new Adafruit_8x16minimatrix();

	// initialize with the right I2C address
	pMatrix8x16->begin(0x70);

	// show successful initialization
	pMatrix8x16->clear();
	pMatrix8x16->setRotation(3);
	pMatrix8x16->setTextSize(1);
	pMatrix8x16->setTextColor(LED_ON);
	pMatrix8x16->setCursor(2, 1);
	pMatrix8x16->print("OK");
	pMatrix8x16->writeDisplay();
	delay(1000);

	// turns on the whole matrix
	pMatrix8x16->fillScreen(LED_ON);
	pMatrix8x16->setRotation(0);
	pMatrix8x16->writeDisplay();
}


/**
 * Sets the brightness of the 8x16 Matrix.
 * Turns on as much LEDs as the brightness is set
 */
void setMatrix8x16Brightness(uint16_t brightness) {

	int column = 0;
	int ledsOn = 0;

	// brightness ranges from 0..255 but there are only 128 LEDs
	brightness++;
	brightness /= 2;

	// clear Matrix
	pMatrix8x16->clear();

	// until enough LEDs are turned on
	while (brightness > 0) {

		ledsOn = 16;

		// are there not enough LEDs left
		if (brightness < 16)
			ledsOn = brightness;

		// turn on the LEDs
		// all ON LEDs will be centered
		pMatrix8x16->drawFastVLine(column, (16 - ledsOn) / 2, ledsOn, LED_ON);

		// subtract the LEDs which has been turned on already
		brightness -= ledsOn;

		column++;
	}

	// show at matrix
	pMatrix8x16->writeDisplay();
}


/**
 * Creates the Adafruit_IS31FL3731_Wing object and runs the initialization.
 */
void initLEDWing105() {
	pLEDWing105 = new Adafruit_IS31FL3731_Wing();

	// initialize
	if (!pLEDWing105->begin())
		error(F("IS31(LEDWing105) not found"));

	log(F("IS31(LEDWing105) successfully connected!"));

	// show successful initialization
	pLEDWing105->clear();
	pLEDWing105->setRotation(2);
	pLEDWing105->setTextSize(1);
	pLEDWing105->setTextColor(64);
	pLEDWing105->setCursor(1, 0);
	pLEDWing105->print("OK");
	delay(1000);

	// resets the rotation and clears the screen
	pLEDWing105->setRotation(0);
	pLEDWing105->clear();
}


/**
 * Sets the brightness of the LED wing 105 Matrix.
 * Sets to brightness of every led to the given value
 */
void setLEDWing105Brightness(uint16_t brightness) {

	// set the brightness
	pLEDWing105->fillScreen(brightness);
}


/**
 * Shows the wave in position index. with the given brightness
 */
void waveLEDWing105(int index, int brightness) {

	float factor = brightness / (float)255;
	int calcBrightness;

	// draw all the lines
	for (int i = 0; i < pLEDWing105->width(); i++) {

		// calculate brightness
		calcBrightness = WAVE[(index + i) % WAVE_LENGTH] * factor;

		// LED[0, 2] is broken -> don't turn on
		if (i != 2)
			pLEDWing105->drawFastVLine(i, 0, 7, calcBrightness);
		else
			pLEDWing105->drawFastVLine(i, 1, 6, calcBrightness);
	}

}
