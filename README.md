# Description
Controls the mounted Adafruit led-wing which is connected to the feather board by using the ```Controller -> Color Picker``` dialog of the Adafruit ```Bluefruit LE Connect``` app. *Only brightness is supported.*

To automatically detect the mounted wing it is necessary to interconnect pin 5 and pin 6 of wing 15x7 wing and not to do so on wing matrix 8x16!

If Bluetooth is never connected the feather board the leds will be switched off after a timeout of 5 minutes.

# Hardware
- [Adafruit Feather M0 Bluefruit LE](https://www.adafruit.com/product/2995)
- [Adafruit 15x7 CharliePlex LED Matrix Display FeatherWing Yellow](https://www.adafruit.com/product/3135)
- [Adafruit 0.8" 8x16 LED Matrix FeatherWing Display Kit - Yellow](https://www.adafruit.com/product/3153)

# Libraries used
- [Adafruit_GFX.h](https://github.com/adafruit/Adafruit-GFX-Library)
- [Adafruit_IS31FL3731.h](https://github.com/adafruit/Adafruit_IS31FL3731)
- [Adafruit_LEDBackpack.h](https://github.com/adafruit/Adafruit_LED_Backpack)
- [Adafruit_BluefruitLE_SPI.h](https://github.com/adafruit/Adafruit_BluefruitLE_nRF51)

# IDE
Sloeber Eclipse IDE v4.2 [eclipse.baeyens.it/](https://eclipse.baeyens.it/)